# IceMicroML

Machine learning applied to ice microphysical processes based on McSnow output. The results are described and discussed in 

Axel Seifert and Christoph Siewert, 2023, **An ML-based P3-like multimodal two-moment ice microphysics in the ICON model**, submitted to *J. Adv. Modeling Earth
Systems*

This Gitlab repository contains only the Python/Tensorflow scripts. The complete training data, which would be necessary to reproduce the paper, can be downloaded from Zenodo.


